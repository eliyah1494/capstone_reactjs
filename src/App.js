import HomePage from "./Pages/HomePage/HomePage";

import DetailPage from "./Pages/DetailPage/DetailPage";
import logo from "./logo.svg";
import "./App.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import Layout from "./HOC/Layout";
import Register from "./Pages/RegisterPage/Register";
import "../src/index.css";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<Register />} />
          <Route />
          <Route
            path="/detail/:idPhim"
            element={
              <Layout>
                <DetailPage />
              </Layout>
            }
          />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
