import moment from "moment";
import React from "react";

export default function MovieItemTab({ movie }) {
  return (
    <div className="flex mt-10 items-center space-x-5">
      <img
        className="h-48 w-32 border rounded-lg "
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        <div>
          <h3 className="font-black">{movie.tenPhim}</h3>
        </div>
        <div className=" grid grid-cols-3 gap-8">
          {movie.lstLichChieuTheoPhim?.slice(0, 6).map((lichChieu) => {
            return (
              <p className="bg-[#EAE7B1] text-black mt-5  px-3 py-2 rounded-xl font-medium">
                {moment(lichChieu.ngayChieuGioChieu).format("DD/MM hh:mm A")}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
}
