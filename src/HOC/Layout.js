import React from "react";
import FooterPage from "../Components/FooterPage/FooterPage";

import Header from "../Components/Header/Header";
import ScrollToTop from "../Components/ScrollToTop/ScrollToTop";

export default function Layout({ children }) {
  return (
    <div>
      <Header />

      <div className="mt-24">{children}</div>
      <FooterPage />
      <ScrollToTop />
    </div>
  );
}
