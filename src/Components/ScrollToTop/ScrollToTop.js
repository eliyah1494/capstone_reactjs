import { BackTop } from "antd";
import React from "react";
import Lottie from "lottie-react";
import back_totop from "../../assets/17824-back-to-top.json";

export default function ScrollToTop() {
  return (
    <div>
      <BackTop>
        <strong style={{ color: "black" }}>
          <Lottie animationData={back_totop} loop={true} />
          <p className="ml-2">TOP</p>
        </strong>
      </BackTop>
    </div>
  );
}
